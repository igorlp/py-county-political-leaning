print('Importing Requirements')
import pandas as pd
import numpy as np
import geopandas as gp

print('Loading Individual Contributions')
indiv_contributions = pd.read_csv('/', sep="|",
                 encoding="ISO-8859-1", keep_default_na=False, low_memory=False, error_bad_lines=False)
print('Loading Committee Information')
committee_data = pd.read_csv('/', sep="|",
                 encoding="ISO-8859-1", keep_default_na=False, low_memory=False, error_bad_lines=False)
print('Loading Zip to County Database')
zip_county_data = pd.read_csv('/', sep=",")


print('Modifying Individual Contributions and Remove Non-US Data')
indiv_contributions = indiv_contributions[indiv_contributions.STATE != 'ZZ']
indiv_contributions = indiv_contributions[indiv_contributions.STATE != '']
indiv_contributions = indiv_contributions[indiv_contributions.ZIP_CODE != '']
indiv_contributions = indiv_contributions[indiv_contributions.ZIP_CODE.str.isdecimal() == True]
indiv_contributions['ZIP_CODE'] = indiv_contributions['ZIP_CODE'].apply(':5'.format)
indiv_contributions['ZIP_CODE'] = indiv_contributions['ZIP_CODE'].astype('int64')

print('Merging All Files')
data = pd.merge(indiv_contributions, committee_data, how='left', left_on='CMTE_ID', right_on='CMTE_ID')
data = pd.merge(data, zip_county_data, how='left', left_on='ZIP_CODE', right_on='ZIP')

print('Exclude Contributions that do not align with either Republicans or Democrats')
data = data[(data.CMTE_PTY_AFFILIATION == 'DEM') | (data.CMTE_PTY_AFFILIATION == 'REP')]

print('Creating Pivot')
results = pd.pivot_table(data, values='TRANSACTION_AMT', columns='CMTE_PTY_AFFILIATION', index='COUNTY', aggfunc=np.sum)

print('Adding information and analysis to Pivot')
results.fillna(0, inplace=True)
results = results.reset_index()
results['COUNTY'] = results['COUNTY'].astype('int64').apply('{:0>5}'.format)
results['DIFFERENCE'] = results['REP'] - results['DEM']
results['LEANING'] = np.sign(results['REP'] - results['DEM'])
results.columns = ['COUNTY_CODE', 'DEM', 'REP', 'DIFFERENCE', 'LEANING']

print('Creating GeoJSON')
geojson = gp.read_file('/')

print('Modifying GeoJSON')
geojson['COUNTY_CODE'] = geojson['STATE'] + geojson['COUNTY']
geojson = geojson.merge(results, on='COUNTY_CODE', how='left')

print('Writing GeoJSON')
geojson.to_file('georesultsdata.geojson', driver='GeoJSON')

print('Done')

